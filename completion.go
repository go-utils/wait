package wait

import "sync"

// A Completion is a completion value, error pair
type Completion interface {
	// Complete marks the completion as complete, setting the value and error, and
	// removes it from the list
	Complete(interface{}, error)
	// Wait waits until the completion is complete and then returns the value and
	// error
	Wait() (interface{}, error)
}

// New returns a new completion
//
// If `cleanup` is specified, it will be called by `Complete()`
func New(cleanup func(interface{}, error)) Completion {
	return &completion{
		cond:    sync.NewCond(&sync.Mutex{}),
		cleanup: cleanup,
	}
}

type completion struct {
	cond  *sync.Cond
	done  bool
	value interface{}
	err   error

	cleanup func(interface{}, error)
}

func (w *completion) Complete(value interface{}, err error) {
	// done is only ever set to true, so this can be done without a lock
	if w.done {
		return
	}

	w.cond.L.Lock()

	// cover the race where someone else completes w before the lock is acquired
	if w.done {
		w.cond.L.Unlock()
		return
	}

	// complete!
	w.done, w.value, w.err = true, value, err

	// call cleanup
	if w.cleanup != nil {
		w.cleanup(value, err)
	}

	// wake everyone
	w.cond.Broadcast()
	w.cond.L.Unlock()
}

func (w *completion) Wait() (interface{}, error) {
	// done is only ever set to true, so this can be done without a lock
	if w.done {
		return w.value, w.err
	}

	// wait for completion
	w.cond.L.Lock()
	for !w.done {
		w.cond.Wait()
	}
	w.cond.L.Unlock()

	// once done is set, these will never change, so no lock is necessary
	return w.value, w.err
}
