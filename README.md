# Go Wait

[![GoDoc](https://godoc.org/gitlab.com/go-utils/wait?status.svg)](http://godoc.org/gitlab.com/go-utils/wait)

This module provides completions and wait lists.

## Completions

A completion is a concurrent value that can be used to notify waiters when the
result of an operation is complete. Calls to `Wait()` will block until
`Complete()` is called. Calling `Complete()` sets the value and error that
`Wait()` will return (first-come, first-served).

## Wait Lists

A wait list is a set of wait items. A wait item is a completion that has an ID.
A wait list can be used to keep track of multiple different asynchronous tasks.
Calling `list.Next()` will create a new wait item. The ID of that item can be
used to retrieve the item from the list. When an item is completed, it is
removed from the list and can no longer be retrieved.