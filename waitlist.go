package wait

import (
	"errors"
	"sync"
	"sync/atomic"
	"unsafe"
)

// ErrClosed indicates that the completion was part of a list that was closed
var ErrClosed = errors.New("the wait list that this completion is associated with has been closed")

// A List is a list of completion items with IDs
type List interface {
	// Find returns the item with the specified ID
	Find(uint64) (Item, bool)
	// Next creates a new wait item, attaches it to the list, and returns it
	Next() Item
	// Close prevents new items from being added and completes any remaining items
	Close()
}

// A Item is a completion item
type Item interface {
	Completion
	// ID returns the item's ID
	ID() uint64
}

// NewList creates and returns a new List
func NewList() List {
	return &waitList{
		done: make(chan struct{}),
	}
}

type waitList struct {
	maxID  uint64
	done   chan struct{}
	adding sync.WaitGroup

	head *waitItem
}

type waitItem struct {
	id         uint64
	next, prev *waitItem
	inner      Completion
}

func (l *waitList) Find(id uint64) (Item, bool) {
	var i = l.head

	for i != nil && i.id != id {
		i = i.next
	}

	if i == nil {
		return nil, false
	}
	return i, true
}

func compareAndSwapList(addr **waitList, old, new *waitList) bool {
	return atomic.CompareAndSwapPointer((*unsafe.Pointer)(unsafe.Pointer(addr)), unsafe.Pointer(old), unsafe.Pointer(new))
}

func compareAndSwapItem(addr **waitItem, old, new *waitItem) bool {
	return atomic.CompareAndSwapPointer((*unsafe.Pointer)(unsafe.Pointer(addr)), unsafe.Pointer(old), unsafe.Pointer(new))
}

func (l *waitList) Next() Item {
	l.adding.Add(1)
	select {
	case <-l.done:
		l.adding.Done()
		panic("wait list has been closed")

	default:
	}

	var id = atomic.AddUint64(&l.maxID, 1)
	var item = &waitItem{id: id}
	item.inner = New(func(_ interface{}, err error) {
		if err == ErrClosed {
			return
		}
		l.remove(item)
	})

	for {
		var head = l.head

		if compareAndSwapItem(&l.head, head, item) {
			if head == nil {
				break
			}

			item.next = head
			head.prev = item
			break
		}
	}

	l.adding.Done()
	return item
}

func (l *waitList) remove(i *waitItem) {
	for {
		var id = i.id
		if id == 0 {
			panic("tried to remove twice")
		}

		if atomic.CompareAndSwapUint64(&i.id, id, 0) {
			break
		}
	}

	// TODO is this sane?
	for {
		if i == l.head {
			if i.next != nil {
				i.next.prev = nil
			}
			if !compareAndSwapItem(&l.head, i, i.next) {
				if i.next != nil {
					i.next.prev = i
				}
				continue
			}

			i.next = nil
			i.prev = nil
			break
		}

		if i.next != nil {
			i.next.prev = i.prev
		}
		if !compareAndSwapItem(&i.prev.next, i, i.next) {
			if i.next != nil {
				i.next.prev = i
			}
			continue
		}

		i.next = nil
		i.prev = nil
		break
	}
}

func (l *waitList) Close() {
	close(l.done)
	l.adding.Wait()

	for i := l.head; i != nil; i = i.next {
		i.Complete(nil, ErrClosed)
	}
}

func (i *waitItem) ID() uint64 {
	return i.id
}

func (i *waitItem) Complete(value interface{}, err error) {
	i.inner.Complete(value, err)
}

func (i *waitItem) Wait() (interface{}, error) {
	return i.inner.Wait()
}
