package wait

import "sync"

// A CompletionWithArgument is a Completion with an argument
type CompletionWithArgument interface {
	Completion
	// Argument returns the completion's argument
	Argument() interface{}
}

type completionWithArg struct {
	completion
	arg interface{}
}

// NewWithArgument returns a new completion with an argument
//
// See `New()`
func NewWithArgument(arg interface{}, cleanup func(interface{}, error)) CompletionWithArgument {
	return &completionWithArg{
		completion{
			cond:    sync.NewCond(&sync.Mutex{}),
			cleanup: cleanup,
		},
		arg,
	}
}

func (w *completionWithArg) Argument() interface{} {
	return w.arg
}
